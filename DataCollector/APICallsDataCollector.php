<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 6.1.18
 * Time: 23.22.
 */

namespace Digibank\ApiClientBundle\DataCollector;

use Digibank\ApiClientBundle\API\Log\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class APICallsDataCollector extends DataCollector
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Constructor.
     *
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'commands' => $this->logger->getCommands(),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function reset()
    {
        $this->data = array();
    }

    /**
     * Returns an array of collected commands.
     *
     * @return array
     */
    public function getCommands()
    {
        return $this->data['commands'];
    }

    /**
     * Returns the number of collected commands.
     *
     * @return int
     */
    public function getCommandCount()
    {
        return count($this->data['commands']);
    }

    /**
     * Returns the number of failed commands.
     *
     * @return int
     */
    public function getErrorCount()
    {
        return count(array_filter($this->data['commands'], function ($command) {
            return false !== $command['error'];
        }));
    }

    /**
     * Returns the number of failed commands.
     *
     * @return int
     */
    public function getCachedCommandCount()
    {
        return count(array_filter($this->data['commands'], function ($command) {
            return true === $command['is_cached'];
        }));
    }

    /**
     * Returns the execution time of all collected commands in seconds.
     *
     * @return float
     */
    public function getTime()
    {
        $time = 0;
        foreach ($this->data['commands'] as $command) {
            $time += $command['execution_time'];
        }

        return $time;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'app.api_calls_collector';
    }
}
