<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 19.11.17
 * Time: 20.52.
 */

namespace Digibank\ApiClientBundle\DependencyInjection;

use Digibank\ApiClientBundle\API\Client;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Reference;

class DigibankApiClientExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('digibank_api_client.api_url', $config['api_url']);
        $container->setParameter('digibank_api_client.api_key', $config['api_key']);
        $container->setParameter('digibank_api_client.redis_namespace', $config['redis_namespace']);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->getDefinition(Client::class)->setArgument('$redis', new Reference('snc_redis.'.$config['redis_client']));
    }
}
