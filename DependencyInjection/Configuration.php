<?php

namespace Digibank\ApiClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('digibank_api_client');

        $rootNode
            ->children()
                ->scalarNode('api_url')->isRequired()->end()
                ->scalarNode('api_key')->isRequired()->end()
                ->scalarNode('redis_client')->defaultValue('default')->end()
                ->scalarNode('redis_namespace')->defaultValue('site_api_cache')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
