<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 13.12.17
 * Time: 22.08.
 */

namespace Digibank\ApiClientBundle\API\Exception;

class InvalidRequestException extends \RuntimeException
{
    private $responseBody;

    public function setResponseBody($body)
    {
        $this->responseBody = $body;
    }

    public function getResponseBody()
    {
        return $this->responseBody;
    }
}
