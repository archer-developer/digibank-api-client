<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 6.1.18
 * Time: 18.13.
 */

namespace Digibank\ApiClientBundle\API\Log;

use Psr\Log\LoggerInterface;

class Logger
{
    protected $logger;
    protected $nbCommands = 0;
    protected $commands = array();

    /**
     * Constructor.
     *
     * @param LoggerInterface $logger A LoggerInterface instance
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Logs a command.
     *
     * @param string      $method   HTTP method
     * @param string      $command  Redis command
     * @param float       $duration Duration in milliseconds
     * @param bool|string $error    Error message or false if command was successful
     * @param bool        $isCached Command was cached
     */
    public function logCommand($method, $command, $duration, $error = false, $isCached = false)
    {
        ++$this->nbCommands;

        if (null !== $this->logger) {
            $this->commands[] = array(
                'method' => $method,
                'cmd' => $command,
                'execution_time' => $duration,
                'error' => $error,
                'is_cached' => $isCached,
            );
            if ($error) {
                $message = 'Command "'.$command.'" failed ('.$error.')';

                $this->logger->error($message);
            } else {
                $this->logger->info('Executing command "'.$command.'"');
            }
        }
    }

    /**
     * Returns the number of logged commands.
     *
     * @return int
     */
    public function getNbCommands()
    {
        return $this->nbCommands;
    }

    /**
     * Returns an array of the logged commands.
     *
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }
}
