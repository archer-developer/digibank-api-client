<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 5.12.17
 * Time: 23.10.
 */

namespace Digibank\ApiClientBundle\API;

use Digibank\ApiClientBundle\API\Exception\InvalidRequestException;
use Digibank\ApiClientBundle\API\Log\Logger;
use Digibank\ApiClientBundle\Model\PaymentAccount;
use Digibank\ApiClientBundle\Model\PaymentAccountCollection;
use Digibank\ApiClientBundle\Model\PaymentAccountPair;
use Digibank\ApiClientBundle\Model\PaymentAccountPairCollection;
use Digibank\CommonBundle\Model\Order;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;
use Predis\Client as RedisClient;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class Client
 * @package Digibank\ApiClientBundle\API
 */
class Client
{
    const WATCH_KEY = 'api_call';

    const API_KEY_HEADER = 'api-key';

    /**
     * @var string
     */
    private $apiUrl;

    /**
     * @var string
     */
    private $apiCacheNamespace;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var RedisClient|null
     */
    private $redis;

    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var Session
     */
    private $session;

    /**
     * Client constructor.
     *
     * @param string           $apiUrl
     * @param string           $apiCacheNamespace
     * @param string           $apiKey
     * @param Logger           $logger
     * @param SessionInterface $session
     * @param null|RedisClient $redis
     */
    public function __construct(
        $apiUrl,
        $apiCacheNamespace,
        $apiKey,
        Logger $logger,
        SessionInterface $session,
        ?RedisClient $redis = null
    ) {
        $this->apiUrl = $apiUrl;
        $this->apiCacheNamespace = $apiCacheNamespace;
        $this->apiKey = $apiKey;
        $this->logger = $logger;
        $this->redis = $redis;
        $this->serializer = SerializerBuilder::create()->build();
        $this->session = $session;
    }

    /**
     * Получить список кошельков.
     *
     * @return PaymentAccountCollection
     */
    public function getAccounts(): PaymentAccountCollection
    {
        $accounts = (array) $this->get('/payment-accounts');
        $accounts = $this->serializer->deserialize(
            json_encode($accounts),
            'array<'.PaymentAccount::class.'>',
            'json'
        );

        return new PaymentAccountCollection($accounts);
    }

    /**
     * Получить доступные пары обмена.
     *
     * @return PaymentAccountPairCollection
     */
    public function getAccountPairs(): PaymentAccountPairCollection
    {
        $pairs = (array) $this->get('/payment-account-pairs', [], false);
        $pairCollection = new PaymentAccountPairCollection();
        foreach ($pairs as $pair) {
            $to = $pair->to;
            //@todo Убрать это преобразования ключа после внесения изменений в ответ API
            array_walk($to, function ($item) {
                $item->account_to_id = $item->account_id;
                unset($item->account_id);
            });
            $to = $this->serializer->deserialize(json_encode($to), 'array<'.PaymentAccountPair::class.'>', 'json');
            $pairCollection->set($pair->id, $to);
        }

        return $pairCollection;
    }

    /**
     * Получить настройки.
     *
     * @return mixed
     */
    public function getConfigVars()
    {
        return $this->get('/config-vars');
    }

    /**
     * Получить курсы для отображения на сайте.
     *
     * @return mixed
     */
    public function getRates()
    {
        return $this->get('/rates', [], false);
    }

    /**
     * Получить список текстовых страниц.
     *
     * @return mixed
     */
    public function getPages()
    {
        return $this->get('/pages');
    }

    /**
     * Получить содержимое текстовой страницы.
     *
     * @param $slug
     *
     * @return mixed
     */
    public function getPage($slug)
    {
        return $this->get('/pages/'.$slug);
    }

    /**
     * Получить список новостей для главной.
     *
     * @param int $limit
     *
     * @return mixed
     */
    public function getMainArticles($limit)
    {
        return $this->get('/news', ['limit' => $limit]);
    }

    /**
     * Получить содержимое новости.
     *
     * @param $slug
     *
     * @return mixed
     */
    public function getArticle($slug)
    {
        return $this->get('/news/'.$slug);
    }

    /**
     * Получить список новостей.
     *
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function getArticles($page, $limit)
    {
        return $this->get('/news/', ['page' => $page, 'limit' => $limit]);
    }

    /**
     * Получить список отзывов.
     *
     * @return array
     */
    public function getReviews()
    {
        return $this->get('/review/');
    }

    /**
     * Получить список пунктов блока "Как это работает".
     *
     * @return array
     */
    public function getHowItWorksItems()
    {
        return $this->get('/how-it-works');
    }

    /**
     * Добавить отзыв.
     *
     * @param $name
     * @param $content
     *
     * @return mixed
     */
    public function addReview($name, $content)
    {
        return $this->post('/review/', [
            'name' => $name,
            'content' => $content,
        ]);
    }

    /**
     * Обновление профиля пользователя.
     *
     * @param string $email
     * @param string $name
     * @param string $phone
     * @param string $password
     *
     * @return mixed
     */
    public function updateUser($email, $name, $phone, $password)
    {
        $response = $this->post('/user/update', [
            'email' => $email,
            'name' => $name,
            'phone' => $phone,
            'password' => $password,
        ]);

        return $response->user;
    }

    /**
     * Возвращает информацию о последних обменах.
     *
     * @param $count
     *
     * @return mixed
     */
    public function getLastOrdersAction($count)
    {
        return $this->get('/orders/last/'.$count);
    }

    /**
     * Создание заявки.
     *
     * @param Order $order
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $ip - IP адрес пользователя, создающего завку
     * @param int $monitor_id - Индентификатор источника перехода (монитор)
     *
     * @return mixed
     */
    public function createOrder(
        Order $order,
        $name,
        $email,
        $phone,
        $ip = null,
        $monitor_id = null
    ) {
        return $this->post('/orders/create', [
            'account_from_id' => $order->getAccountFromId(),
            'account_to_id' => $order->getAccountToId(),
            'sum_from' => $order->getSumFrom(),
            'sum_to' => $order->getSumTo(),
            // комиссия обмена. Если не передана, то будет взята комиссия на момент создания заявки
            'commission' => $order->getCommission(),
            'fields_from' => $order->getFieldsFrom(),
            'fields_to' => $order->getFieldsTo(),
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'ip' => $ip,
            'monitor_id' => $monitor_id,
        ]);
    }

    /**
     * Создание нового пользователя.
     *
     * @param string $name
     * @param string $email
     * @param string $phone
     * @param string $password
     * @param int    $partner_id
     *
     * @return mixed
     */
    public function createUser($name, $email, $phone, $password, $partner_id)
    {
        return $this->post('/user/create', [
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'password' => $password,
            'partner_id' => $partner_id,
        ]);
    }

    /**
     * Активация пользователя.
     *
     * @param string $token
     *
     * @return mixed
     */
    public function confirmUser($token)
    {
        return $this->post('/user/confirm', [
            'token' => $token,
        ]);
    }

    /**
     * Авторизация пользователя.
     *
     * @param string $login
     * @param string $password
     *
     * @return mixed
     */
    public function loginUser($login, $password)
    {
        $response = $this->post('/user/check', [
            'username' => $login,
            'password' => $password,
        ]);

        return $response->user;
    }

    /**
     * Запрос на восстанавление пароля.
     *
     * @param $email
     *
     * @return mixed
     */
    public function passwordRestoreRequest($email)
    {
        return $this->post('/user/password-restore-request', [
            'email' => $email,
        ]);
    }

    /**
     * Получение нового пароля.
     *
     * @param string $token
     *
     * @return mixed
     */
    public function passwordRestore($token)
    {
        return $this->post('/user/password-restore', [
            'token' => $token,
        ]);
    }

    /**
     * Получение списка заявок пользователя.
     *
     * @param string $email
     * @param int    $limit
     * @param int    $offset
     *
     * @return mixed
     */
    public function getUserOrders($email, $limit, $offset)
    {
        /*
         * @todo Должен кешироваться, но должен сбрасываться кеш
         * заявок конкретного пользователя при измении сущности зявки
         */
        return $this->get('/orders/', [
            'email' => $email,
            'limit' => $limit,
            'offset' => $offset,
        ], false);
    }

    /**
     * Пометить заяку как оплаченную.
     *
     * @param int    $orderId
     * @param string $userEmail
     *
     * @return mixed
     */
    public function markOrderAsPaid($orderId, $userEmail)
    {
        return $this->post('/orders/mark-as-paid', [
            'order_id' => $orderId,
            'user_email' => $userEmail,
        ]);
    }

    /**
     * Получить информацию по рефералам пользователя.
     *
     * @param int $userId
     *
     * @return object
     */
    public function getReferralInfo($userId)
    {
        /*
         * @todo Должен кешироваться, по времени
         */
        return $this->get('/partner/info/'.$userId, [], false);
    }

    /**
     * Получить информацию по всем начислениям от рефералов пользователя.
     *
     * @param int $userId
     * @param int $offset
     *
     * @return array
     */
    public function getReferralsCompletedOrders($userId, $offset = 0)
    {
        return $this->get('/partner/referrals-completed-orders/'.$userId.'/'.$offset, [], false);
    }

    /**
     * Получить кошельки, на которые можно выводить прибыль.
     *
     * @return array
     */
    public function getAccountsForProfit()
    {
        return $this->get('/partner/accounts-for-profit');
    }

    /**
     * Создать заявку на вывод прибыли.
     *
     * @param int    $userId
     * @param float  $amount
     * @param int    $accountId
     * @param string $accountDetails
     *
     * @return array
     */
    public function createUserInvoice($userId, $amount, $accountId, $accountDetails)
    {
        return $this->post('/partner/create-user-invoice/'.$userId, [
            'account_id' => $accountId,
            'account_details' => $accountDetails,
            'amount' => $amount,
        ]);
    }

    /**
     * Получить данные по реферальной программе монитора.
     *
     * @param int $userId
     * @return array
     */
    public function getMonitorInfo($userId)
    {
        return $this->get('/monitor/'.$userId);
    }

    /**
     * @param string $url
     * @param array  $params
     * @param bool   $useCache
     *
     * @return mixed
     */
    private function get($url, $params = [], $useCache = true)
    {
        $watcher = new Stopwatch();
        $watcher->start(self::WATCH_KEY);

        $params['__state'] = json_encode($this->session->get('api_request_state', []));

        $url = $this->apiUrl.$url;
        $url .= '?'.http_build_query($params);

        // Кэш первого уровня
        if (isset($this->cache[$url])) {
            $event = $watcher->stop(self::WATCH_KEY);
            $this->logger->logCommand('GET', $url, $event->getDuration(), false, true);

            return $this->cache[$url];
        }
        // Кэш второго уровня
        $decodedBody = null;
        $cached = false;
        if ($useCache && $this->redis) {
            if ($this->redis->hexists($this->apiCacheNamespace, $url)) {
                $cached = true;
                $decodedBody = unserialize($this->redis->hget($this->apiCacheNamespace, $url));
            }
        }

        if ($decodedBody === null) {
            $client = new \GuzzleHttp\Client();
            $client->setDefaultOption('connect_timeout', 5);
            $client->setDefaultOption('timeout', 10);

            // Ключ API передается в заголовках запроса, чтобы избежать попадания в логи и т.д.
            $headers = [
                self::API_KEY_HEADER => $this->apiKey,
            ];

            $response = $client->get($url, ['headers' => $headers]);

            $decodedBody = json_decode($response->getBody());
            if (!$decodedBody || 'success' != $decodedBody->status) {
                $errorMessage = 'Invalid API response (status '.
                    $response->getStatusCode().'):'.
                    $response->getBody()
                ;

                $event = $watcher->stop(self::WATCH_KEY);
                $this->logger->logCommand('GET', $url, $event->getDuration(), $errorMessage);
                throw new \RuntimeException($errorMessage);
            }

            if ($useCache) {
                $this->redis->hset($this->apiCacheNamespace, $url, serialize($decodedBody));
            }
        }

        $this->session->set('api_request_state', $decodedBody->state);

        $event = $watcher->stop(self::WATCH_KEY);
        $this->logger->logCommand('GET', $url, $event->getDuration(), false, $cached);

        $this->cache[$url] = $decodedBody->response;

        return $this->cache[$url];
    }

    /**
     * @param $url
     * @param $params
     *
     * @return mixed
     */
    private function post($url, $params)
    {
        $watcher = new Stopwatch();
        $watcher->start(self::WATCH_KEY);

        $params['api_key'] = $this->apiKey;
        $params['__state'] = json_encode($this->session->get('api_request_state', []));

        $client = new \GuzzleHttp\Client();
        $client->setDefaultOption('connect_timeout', 5);
        $client->setDefaultOption('timeout', 10);

        // Ключ API передается в заголовках запроса, чтобы избежать попадания в логи и т.д.
        $headers = [
            self::API_KEY_HEADER => $this->apiKey,
        ];

        $response = $client->post($this->apiUrl.$url, ['body' => $params, 'headers' => $headers]);

        $decodedBody = json_decode($response->getBody());
        if (empty($decodedBody)) {
            $error = 'Invalid API Response (status '.
                $response->getStatusCode().'): '.
                $response->getBody()
            ;

            $event = $watcher->stop(self::WATCH_KEY);
            $this->logger->logCommand('POST', $url, $event->getDuration(), $error);

            throw new \RuntimeException($error);
        }
        if ('success' != $decodedBody->status) {
            $event = $watcher->stop(self::WATCH_KEY);
            $error = $decodedBody->response->error_message;
            if (!is_string($error)) {
                $error = print_r($error, true);
            }
            $this->logger->logCommand('POST', $url, $event->getDuration(), $error);

            $e = new InvalidRequestException('API Error');
            $e->setResponseBody($decodedBody->response->error_message);
            throw $e;
        }

        $this->session->set('api_request_state', $decodedBody->state);

        $event = $watcher->stop(self::WATCH_KEY);
        $this->logger->logCommand('POST', $url, $event->getDuration());

        return $decodedBody->response;
    }
}
