<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/14/18
 * Time: 4:39 PM
 */

namespace Digibank\ApiClientBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class PaymentAccountCollection extends ArrayCollection
{
    use JsonSerializableTrait;

    public function __construct(array $elements = [])
    {
        $accounts = [];
        foreach ($elements as $element) {
            if (!$element instanceof PaymentAccount) {
                throw new \RuntimeException(sprintf('Invalid element %s. PaymentAccountCollection must contains PaymentAccount objects only!', get_class($element)));
            }
            $accounts[$element->getId()] = $element;
        }

        parent::__construct($accounts);
    }
}