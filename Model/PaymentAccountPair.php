<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/14/18
 * Time: 4:39 PM
 */

namespace Digibank\ApiClientBundle\Model;

use JMS\Serializer\Annotation as Serializer;

class PaymentAccountPair
{
    /**
     * @Serializer\Type("int")
     */
    private $accountFromId;

    /**
     * @Serializer\Type("int")
     */
    private $accountToId;

    /**
     * @Serializer\Type("float")
     */
    private $commission;

    /**
     * @Serializer\Type("float")
     */
    private $rate;

    /**
     * @Serializer\Type("int")
     */
    private $rateFactor;

    /**
     * @Serializer\Type("float")
     */
    private $minAmountFrom;

    /**
     * @Serializer\Type("float")
     */
    private $minAmountTo;

    /**
     * @Serializer\Type("float")
     */
    private $maxAmountTo;

    /**
     * @return mixed
     */
    public function getAccountFromId()
    {
        return $this->accountFromId;
    }

    /**
     * @return mixed
     */
    public function getMaxAmountTo()
    {
        return $this->maxAmountTo;
    }

    /**
     * @return mixed
     */
    public function getMinAmountTo()
    {
        return $this->minAmountTo;
    }

    /**
     * @return mixed
     */
    public function getMinAmountFrom()
    {
        return $this->minAmountFrom;
    }

    /**
     * @return mixed
     */
    public function getRateFactor()
    {
        return $this->rateFactor;
    }

    /**
     * @return mixed
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @return mixed
     */
    public function getCommission()
    {
        return $this->commission;
    }

    /**
     * @return mixed
     */
    public function getAccountToId()
    {
        return $this->accountToId;
    }
}