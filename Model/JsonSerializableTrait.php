<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/15/18
 * Time: 5:06 PM
 */

namespace Digibank\ApiClientBundle\Model;

use JMS\Serializer\SerializerBuilder;

trait JsonSerializableTrait
{
    public function toJson()
    {
        $serializer = SerializerBuilder::create()->build();

        return $serializer->serialize($this->toArray(), 'json');
    }
}