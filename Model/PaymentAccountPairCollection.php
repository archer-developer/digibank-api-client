<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/14/18
 * Time: 4:39 PM
 */

namespace Digibank\ApiClientBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class PaymentAccountPairCollection extends ArrayCollection
{
    use JsonSerializableTrait;
}