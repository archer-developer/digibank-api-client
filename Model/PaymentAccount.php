<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11/14/18
 * Time: 4:39 PM
 */

namespace Digibank\ApiClientBundle\Model;

use JMS\Serializer\Annotation as Serializer;

class PaymentAccount
{
    /**
     * @Serializer\Type("int")
     */
    private $id;

    /**
     * @Serializer\Type("string")
     */
    private $name;

    /**
     * @Serializer\Type("string")
     */
    private $currency;

    /**
     * @Serializer\Type("string")
     */
    private $iconBase64;

    /**
     * @Serializer\Type("int")
     */
    private $limit;

    /**
     * @Serializer\Type("array")
     */
    private $fields;

    /**
     * @Serializer\Type("string")
     */
    private $bcCode;

    /**
     * @Serializer\Type("bool")
     */
    private $isCash;

    /**
     * @Serializer\Type("bool")
     */
    private $isEmpty;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return mixed
     */
    public function getIconBase64()
    {
        return $this->iconBase64;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @return mixed
     */
    public function getBcCode()
    {
        return $this->bcCode;
    }

    /**
     * @return mixed
     */
    public function getIsCash()
    {
        return $this->isCash;
    }

    /**
     * @return mixed
     */
    public function getIsEmpty()
    {
        return $this->isEmpty;
    }
}