<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11.5.18
 * Time: 23.25
 */

namespace Digibank\ApiClientBundle\Exporter;

interface RateExporterInterface
{
    public function export(): string;
}