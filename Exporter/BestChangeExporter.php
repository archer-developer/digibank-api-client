<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 11.5.18
 * Time: 23.23
 */

namespace Digibank\ApiClientBundle\Exporter;

use Digibank\ApiClientBundle\API\Client;
use Digibank\ApiClientBundle\Model\PaymentAccount;
use Digibank\ApiClientBundle\Model\PaymentAccountPair;

class BestChangeExporter implements RateExporterInterface
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * BestChangeExporter constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Export xml file with rates for BestChange
     *
     * @return string
     */
    public function export(): string
    {
        $accounts = $this->client->getAccounts();
        $accountPairs = $this->client->getAccountPairs();

        $writer = new \XMLWriter();
        $writer->openMemory();
        $writer->startDocument('1.0', 'UTF-8');
        $writer->setIndent(4);
        $writer->startElement('rates');

        /**
         * @var PaymentAccountPair $to
         */
        foreach ($accountPairs as $fromId => $pairs) {
            foreach ($pairs as $to) {
                /**
                 * @var PaymentAccount $accountFrom
                 * @var PaymentAccount $accountTo
                 */
                $accountFrom = $accounts->get($to->getAccountFromId());
                $accountTo = $accounts->get($to->getAccountToId());
                // Если кошелек удален, то пропускаем его
                if (!$accountFrom || !$accountTo) {
                    continue;
                }
                $writer->startElement('item');
                $writer->startElement('from');
                $writer->text($accountFrom->getBcCode());
                $writer->endElement();
                $writer->startElement('to');
                $writer->text($accountTo->getBcCode());
                $writer->endElement();
                $writer->startElement('in');
                $writer->text($to->getRateFactor());
                $writer->endElement();
                $writer->startElement('out');
                $writer->text(sprintf('%.10f', $to->getRate() * $to->getRateFactor()));
                $writer->endElement();
                $writer->startElement('amount');
                $writer->text(sprintf('%.2f', $accountTo->getLimit()));
                $writer->endElement();
                $writer->startElement('minamount');
                $writer->text($to->getMinAmountFrom().' '.$accountFrom->getCurrency());
                $writer->endElement();
                $writer->startElement('maxamount');
                $writer->text(sprintf('%.2f', $accountTo->getLimit() / $to->getRate()).' '.$accountFrom->getCurrency());
                $writer->endElement();
                $writer->startElement('city');
                $writer->text('MSK');
                $writer->endElement();
                $writer->endElement();
            }
        }

        $writer->endElement();
        $writer->endDocument();

        return $writer->outputMemory(true);
    }
}